/**
 * 
 */
package com.botique.crm.security.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.security.web.header.writers.DelegatingRequestMatcherHeaderWriter;
import org.springframework.security.web.header.writers.frameoptions.XFrameOptionsHeaderWriter;
import org.springframework.security.web.header.writers.frameoptions.XFrameOptionsHeaderWriter.XFrameOptionsMode;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;

/**
 * @author user
 *
 */
@SuppressWarnings("deprecation")
@Configuration
@EnableWebMvcSecurity
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

/*** Because authentication is handled outside the application we don�t have to authorize any requests
*/
	protected void configure(HttpSecurity http) throws Exception {
		RequestMatcher matcher = new AntPathRequestMatcher("/adminLogin");
	    DelegatingRequestMatcherHeaderWriter headerWriter =
	        new DelegatingRequestMatcherHeaderWriter(matcher,new XFrameOptionsHeaderWriter());
	http.authorizeRequests().antMatchers("/**").permitAll()
	.and().csrf().disable().logout().and().headers().httpStrictTransportSecurity().and().frameOptions().and().xssProtection()
	.and().addHeaderWriter(new XFrameOptionsHeaderWriter(XFrameOptionsMode.SAMEORIGIN)).addHeaderWriter(headerWriter);
	http.sessionManagement().maximumSessions(1);
	
	/*.requireCsrfProtectionMatcher(new RequestMatcher() {
		private RegexRequestMatcher apiMatcherPaymentSuccess = new RegexRequestMatcher("/MySchool/PaymentSuccess", null);
		private RegexRequestMatcher apiMatcherPaymentFailure = new RegexRequestMatcher("/MySchool/PaymentFailure", null);
		private RegexRequestMatcher defaultRequest = new RegexRequestMatcher("/", null);
		@Override
		public boolean matches(HttpServletRequest request) {
			if(apiMatcherPaymentSuccess.matches(request) || apiMatcherPaymentFailure.matches(request)){
                return false;
			}
			if(defaultRequest.matches(request)){
			return false;
			}
			return true;
		}
	})*/
}
}
