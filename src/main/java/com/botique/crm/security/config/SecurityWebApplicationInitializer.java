/**
 * 
 */
package com.botique.crm.security.config;

import org.springframework.core.annotation.Order;
import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * @author user
 *
 */
@Order(2)
public class SecurityWebApplicationInitializer extends AbstractSecurityWebApplicationInitializer {}
/**
* This WebApplicationInitializer register its security filters on the Application
*
*/
