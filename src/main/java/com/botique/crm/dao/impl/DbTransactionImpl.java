/**
 * 
 */
package com.botique.crm.dao.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.message.MessageFormatMessageFactory;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.botique.crm.dao.DbTransaction;
import com.botique.crm.entity.SDO;
import com.botique.crm.util.CommonUtils;
import com.botique.crm.util.FilterCreator;
import com.botique.crm.util.QueryObject;

/**
 * @author vaisakh ps
 *
 */
@Repository
@Transactional
public class DbTransactionImpl implements DbTransaction {

	private static final Logger logger = LogManager.getLogger(DbTransactionImpl.class,
			new MessageFormatMessageFactory());

	@Autowired
	private SessionFactory sessionFactory;

	Session session = null;
	Transaction tx = null;

	public Transaction beginTransaction() throws Exception {

		try {
			session = sessionFactory.openSession();

			tx = session.beginTransaction();

			logger.info("Successfully started the transaction");
		} catch (Exception ex) {
			throw new Exception("Error occured while starting the transaction", ex);
		}
		return tx;
	}

	public void rollBackTransaction() throws Exception {

		try {
			tx.rollback();

			logger.info("Successfully rolled back the transaction");
		} catch (Exception ex) {
			throw new Exception("Error occured while rollback the transaction", ex);
		} finally {
			try {
				if (session.isOpen())
					session.close();
			} catch (Exception e) {
				logger.error("Error while closing the sessionFactory ", e);
			}
		}
	}

	public void commitTransaction() throws Exception {

		try {
			tx.commit();

			logger.info("Successfully commited the transaction");

		} catch (Exception ex) {
			throw new Exception("Error occured while commiting the transaction", ex);
		} finally {
			try {
				if (session.isOpen())
					session.close();
			} catch (Exception e) {
				logger.error("Error while closing the sessionFactory ", e);
			}
		}
	}

	public SDO persist(SDO entity) throws Exception {

		boolean isSessionNull = false;
		try {
			if (!CommonUtils.isObjectNotEmpty(session)) {
				session = sessionFactory.openSession();
				isSessionNull = true;
			} else if (!session.isOpen()) {
				session = sessionFactory.openSession();
				isSessionNull = true;
			}
			session.persist(entity);
			session.flush();

			logger.info("Successfully persisted the records");
		} catch (Exception ex) {
			throw new Exception("Error occured while persisting the records", ex);
		} finally {
			try {
				if (isSessionNull && session.isOpen())
					session.close();
			} catch (Exception e) {
				logger.error("Error while closing the sessionFactory ", e);
			}
		}
		return entity;
	}

	public void delete(SDO entity) throws Exception {

		boolean isSessionNull = false;
		try {
			if (!CommonUtils.isObjectNotEmpty(session)) {
				session = sessionFactory.openSession();
				isSessionNull = true;
			} else if (!session.isOpen()) {
				session = sessionFactory.openSession();
				isSessionNull = true;
			}
			session.delete(session.merge(entity));

			logger.info("Successfully deleted the records");
		} catch (Exception ex) {
			throw new Exception("Error occured while deleting the records", ex);
		} finally {
			try {
				if (isSessionNull && session.isOpen())
					session.close();
			} catch (Exception e) {
				logger.error("Error while closing the sessionFactory ", e);
			}
		}

	}

	public SDO merge(SDO entity) throws Exception {

		boolean isSessionNull = false;
		try {
			if (!CommonUtils.isObjectNotEmpty(session)) {
				session = sessionFactory.openSession();
				isSessionNull = true;
			} else if (!session.isOpen()) {
				session = sessionFactory.openSession();
				isSessionNull = true;
			}
			session.merge(entity);
			session.flush();

			logger.info("Successfully merged the records");
		} catch (Exception ex) {
			throw new Exception("Error occured while merging the records", ex);
		} finally {
			try {
				if (isSessionNull && session.isOpen())
					session.close();
			} catch (Exception e) {
				logger.error("Error while closing the sessionFactory ", e);
			}
		}
		return entity;
	}

	public Object forceExecuteQuery(QueryObject queryObj, FilterCreator filterCreator) throws Exception {

		boolean isSessionNull = false;
		try {
			if (!CommonUtils.isObjectNotEmpty(session)) {
				session = sessionFactory.openSession();
				isSessionNull = true;
			} else if (!session.isOpen()) {
				session = sessionFactory.openSession();
				isSessionNull = true;
			}
			String strQuery = (String) forceCreateQuery(queryObj);

			Query query = session.createQuery(strQuery);
			if (CommonUtils.isObjectNotEmpty(filterCreator)) {
				if (filterCreator.isFilterEnabled()) {
					query.setFirstResult(filterCreator.getMinLength());
					query.setMaxResults(filterCreator.getMaxLength());
				}
			}
			if (CommonUtils.isObjectNotEmpty(queryObj.getBindingData())) {
				for (int i = 0; i < queryObj.getBindingData().length; i++) {
					query.setParameter(i, queryObj.getBindingData()[i]);

				}
			}
			Object list = query.list();

			logger.debug("Query : {0}", strQuery);
			return list;
		} catch (Exception e) {

			throw new Exception("Error occured while executing the query", e);

		} finally {
			try {
				if (isSessionNull && session.isOpen())
					session.close();
			} catch (Exception e) {
				logger.error("Error while closing the sessionFactory ", e);
			}
		}
	}

	public Object forceCreateQuery(QueryObject queryObj) throws Exception {

		try {

			String strSelectClause = queryObj.getSelectClause();
			String strFromClause = queryObj.getFromClause();
			String strWhereClause = queryObj.getWhereClause();
			String strOrderByClause = queryObj.getOrderBy();
			String strGroupByClause = queryObj.getGroupBy();
			String strQuery;
			if (!CommonUtils.isStringNotEmpty(strFromClause)) {

				throw new Exception("Required Attributes Missing.From Clause is empty");
			}
			if (!CommonUtils.isStringNotEmpty(strSelectClause)) {

				throw new Exception("Required Attributes Missing.Select Clause is empty");
			}

			strQuery = "SELECT " + strSelectClause + " FROM " + strFromClause;

			if (CommonUtils.isStringNotEmpty(strWhereClause)) {
				strQuery = strQuery + " WHERE " + strWhereClause;
			}
			if (CommonUtils.isStringNotEmpty(strOrderByClause)) {
				strQuery = strQuery + " ORDER BY " + strOrderByClause;
			}
			if (CommonUtils.isStringNotEmpty(strGroupByClause)) {
				strQuery = strQuery + " GROUP BY " + strGroupByClause;
			}
			logger.debug("Query : {0}", strQuery);

			return strQuery;
		} catch (Exception e) {

			throw new Exception("Exception Occured during creating the query", e);

		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<SDO> getEntityList(String entity, FilterCreator filterCreator) throws Exception {
		List<SDO> listUser;
		boolean isSessionNull = false;
		try {
			if (!CommonUtils.isObjectNotEmpty(session)) {
				session = sessionFactory.openSession();
				isSessionNull = true;
			} else if (!session.isOpen()) {
				session = sessionFactory.openSession();
				isSessionNull = true;
			}
			Class cls = Class.forName("com.botique.crm.entity." + entity);
			listUser = (List<SDO>) session.createCriteria(cls).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
					.list();
		} catch (Exception e) {

			throw new Exception("Error occured while executing the query", e);

		} finally {
			try {
				if (isSessionNull && session.isOpen())
					session.close();
			} catch (Exception e) {
				logger.error("Error while closing the sessionFactory ", e);
			}
		}
		return listUser;
	}

}
