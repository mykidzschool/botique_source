/**
 * api service for handling the CURD Operations
 */
package com.botique.crm.dao;

import java.util.List;

import org.hibernate.Transaction;

import com.botique.crm.entity.SDO;
import com.botique.crm.util.FilterCreator;
import com.botique.crm.util.QueryObject;

/**
 * @author vaisakh ps
 *
 */
public interface DbTransaction {
	public Transaction beginTransaction() throws Exception;
	public void rollBackTransaction() throws Exception;
	public void commitTransaction() throws Exception;
	public SDO persist(SDO entity) throws Exception;
	public void delete(SDO entity) throws Exception;
	public SDO merge(SDO entity) throws Exception;
	public Object forceExecuteQuery(QueryObject queryObj,FilterCreator filterCreator) throws Exception ;
	public Object forceCreateQuery(QueryObject queryObj)throws Exception;
	public List<SDO> getEntityList(String entity,FilterCreator filterCreator) throws Exception;
}
