/*
 * Class for Setting and getting the query objects
 */
package com.botique.crm.util;

import java.io.Serializable;


public class QueryObject
implements Serializable
{

	private final static long serialVersionUID = 1L;
	protected String fromClause;
	protected String whereClause;
	protected String selectClause;
	protected String orderBy;
	protected String groupBy;
	protected Object[] bindingData;

	/**
	 * Gets the value of the fromClause property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 * 
	 */
	
	public String getFromClause() {
		return fromClause;
	}

	/**
	 * Sets the value of the fromClause property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 * 
	 */
	public void setFromClause(String value) {
		this.fromClause = value;
	}

	/**
	 * Gets the value of the whereClause property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 * 
	 */
	public String getWhereClause() {
		return whereClause;
	}

	/**
	 * Sets the value of the whereClause property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 * 
	 */
	public void setWhereClause(String value) {
		this.whereClause = value;
	}

	/**
	 * Gets the value of the selectClause property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 * 
	 */
	
	public String getSelectClause() {
		return selectClause;
	}

	/**
	 * Sets the value of the selectClause property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 * 
	 */
	public void setSelectClause(String value) {
		this.selectClause = value;
	}

	/**
	 * Gets the value of the orderBy property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 * 
	 */
	public String getOrderBy() {
		return orderBy;
	}

	/**
	 * Sets the value of the orderBy property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 * 
	 */
	public void setOrderBy(String value) {
		this.orderBy = value;
	}

	/**
	 * Gets the value of the groupBy property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 * 
	 */
	public String getGroupBy() {
		return groupBy;
	}

	/**
	 * Sets the value of the groupBy property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 * 
	 */
	public void setGroupBy(String value) {
		this.groupBy = value;
	}

	/**
	 * Gets the value of the bindingData property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link Object }
	 * 
	 */
	public Object[] getBindingData() {
		return bindingData;
	}

	/**
	 * Sets the value of the bindingData property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link Object }
	 * 
	 */
	public void setBindingData(Object... value) {
		this.bindingData = value;
	}

}
