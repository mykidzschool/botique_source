/**
 * 
 */
package com.botique.crm.util;

/**
 * @author user
 *
 */
public class FilterCreator {
	
	private boolean isFilterEnabled;
	private Integer minLength;
	private Integer maxLength;
	/**
	 * @return the isFilterEnabled
	 */
	public boolean isFilterEnabled() {
		return isFilterEnabled;
	}
	/**
	 * @param isFilterEnabled the isFilterEnabled to set
	 */
	public void setFilterEnabled(boolean isFilterEnabled) {
		this.isFilterEnabled = isFilterEnabled;
	}
	/**
	 * @return the minLength
	 */
	public Integer getMinLength() {
		return minLength;
	}
	/**
	 * @param minLength the minLength to set
	 */
	public void setMinLength(Integer minLength) {
		this.minLength = minLength;
	}
	/**
	 * @return the maxLength
	 */
	public Integer getMaxLength() {
		return maxLength;
	}
	/**
	 * @param maxLength the maxLength to set
	 */
	public void setMaxLength(Integer maxLength) {
		this.maxLength = maxLength;
	}
	
	

}
