/**
 * 
 */
package com.botique.crm.util;

import java.util.Collection;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

/**
 * @author Amma
 *
 */
public class CommonUtils {

	public static boolean isStringNotEmpty(String s) {
		return s != null && StringUtils.isNotEmpty(s);
	}

	public static boolean isCollectionNotEmpty(Collection<?> c) {
		return c != null && !c.isEmpty();
	}

	public static boolean isMapNotEmpty(Map<?, ?> m) {
		return m != null && !m.isEmpty();
	}

	public static boolean isObjectNotEmpty(Object o) {
		return o != null;
	}
}
