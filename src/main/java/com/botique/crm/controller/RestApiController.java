/**
 * 
 */
package com.botique.crm.controller;

import java.util.HashMap;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.message.MessageFormatMessageFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.botique.crm.dao.DbTransaction;
import com.botique.crm.db.service.DbTransactionService;
import com.botique.crm.entity.Table1;
import com.botique.crm.entity.Table2;
import com.botique.crm.entity.Table3;
import com.botique.crm.util.FilterCreator;
import com.botique.crm.util.QueryObject;

/**
 * @author user
 * 
 */
@RestController
@RequestMapping("/new/api")
public class RestApiController {

	private static final Logger logger = LogManager.getLogger(RestApiController.class,
			new MessageFormatMessageFactory());
	
	@Autowired
	private DbTransaction dbTransaction;
	
	@Autowired
	private DbTransactionService dbTransactionService;

	@RequestMapping(value = "/log", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public ResponseEntity<HashMap<String, Object>> mySchoolParentLogin() throws Exception {
		HashMap<String, Object> outData = new HashMap<String, Object>();
		Transaction transaction = null;
		try {
			logger.debug("entered into the service");
			transaction = dbTransaction.beginTransaction();
			QueryObject queryObj = new QueryObject();
			queryObj.setSelectClause("t");
			queryObj.setFromClause("Table1 t");
			queryObj.setWhereClause("t.id=? and t.val=?");
			queryObj.setBindingData(7,"val1");
			List<Table1> l =  (List<Table1>) dbTransactionService.select(queryObj, new FilterCreator());
			Table1 table1 = l.get(0);
;			table1.setVal("asfdsagfg");
			Table2 table2 = new Table2();
			table2.setVal("val");
			Table3 table3 = new Table3();
			table3.setVal("val");
			dbTransaction.merge(table1);
			dbTransaction.persist(table2);
			dbTransaction.persist(table3);
			dbTransaction.commitTransaction();
			outData.put("success", "0");
			outData.put("error", "1");
			return new ResponseEntity<HashMap<String, Object>>(outData, HttpStatus.OK);
		} catch (Exception ex) {
			dbTransaction.rollBackTransaction();
			ex.printStackTrace();
		}
		return new ResponseEntity<HashMap<String, Object>>(outData, HttpStatus.OK);
	}

}
