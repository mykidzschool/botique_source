/**
 * 
 */
package com.botique.crm.exception;

/**
 * @author vaisakh ps
 *
 */
public class DbTransactionServiceException extends Exception{
	/**
	 * 
	 */
	private static final long serialVersionUID = -4310346583691071460L;
	private String view;
	@Override
	public String getMessage() {
		return super.getMessage();
	}

	public DbTransactionServiceException(String message, Throwable exception,String view) {
		super((exception != null && (message == null || message.length() == 0)) ? exception.getMessage() : message, exception);
		this.view = view;
	}

	@Override
	public String toString() {
		return super.toString();
	}
	
	/**
	 * @return the view
	 */
	public String getView() {
		return view;
	}

	/**
	 * @param view the view to set
	 */
	public void setView(String view) {
		this.view = view;
	}
	
}
