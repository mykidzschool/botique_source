/**
 * 
 */
package com.botique.crm.db.service;

import java.util.List;

import com.botique.crm.entity.SDO;
import com.botique.crm.exception.DbTransactionServiceException;
import com.botique.crm.util.FilterCreator;
import com.botique.crm.util.QueryObject;

/**
 * @author user
 *
 */
public interface DbTransactionService {
	public Object select(QueryObject queryObj,FilterCreator filterCreator) throws DbTransactionServiceException;
	public void delete(SDO entity,FilterCreator filterCreator) throws DbTransactionServiceException,Exception ;
	public SDO insert(SDO entity,FilterCreator filterCreator)throws DbTransactionServiceException,Exception;
	public SDO update(SDO entity,FilterCreator filterCreator) throws DbTransactionServiceException,Exception;
	public List<SDO> selectList(String entity,FilterCreator filterCreator) throws DbTransactionServiceException;
}
