/**
 * 
 */
package com.botique.crm.db.service.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.message.MessageFormatMessageFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.botique.crm.dao.DbTransaction;
import com.botique.crm.dao.impl.DbTransactionImpl;
import com.botique.crm.db.service.DbTransactionService;
import com.botique.crm.entity.SDO;
import com.botique.crm.exception.DbTransactionServiceException;
import com.botique.crm.util.CommonUtils;
import com.botique.crm.util.FilterCreator;
import com.botique.crm.util.QueryObject;

/**
 * @author vaisakh ps
 *
 */
@Service
public class DbTransactionServiceImpl implements DbTransactionService {

	private static final Logger logger = LogManager.getLogger(DbTransactionImpl.class,
			new MessageFormatMessageFactory());

	@Autowired
	private DbTransaction dbTransaction;

	
	public Object select(QueryObject queryObj, FilterCreator filterCreator) throws DbTransactionServiceException {

		Object returnObj = null;
		logger.debug("Entered into service {0} ", "select");

		try {
			String strSelectClause = queryObj.getSelectClause().trim();
			String strFromClause = queryObj.getFromClause().trim();

			if (!CommonUtils.isStringNotEmpty(strFromClause)) {

				throw new Exception("Required Attributes Missing.From Clause is empty");
			}
			if (!CommonUtils.isStringNotEmpty(strSelectClause)) {

				throw new Exception("Required Attributes Missing.Select Clause is empty");
			}

			returnObj = dbTransaction.forceExecuteQuery(queryObj, filterCreator);
			logger.debug("Successfully completed the query execution");

		} catch (DbTransactionServiceException dbSelect) {

			logger.error("Error occurred while selecting the record", dbSelect);
		} catch (Exception e) {

			logger.error("Error occurred while selecting the record", e);
		}

		return returnObj;
	}

	
	public void delete(SDO entity, FilterCreator filterCreator) throws DbTransactionServiceException, Exception {

		logger.debug("Entered into service {0} ", "delete");
		
		try {
			logger.info("Before deleting...");

			//dbTransaction.beginTransaction();
			dbTransaction.delete(entity);
			//dbTransaction.commitTransaction();

			logger.info("After deleting...");
		} catch (DbTransactionServiceException dbDelete) {

			logger.error("Error occurred while deleting the record", dbDelete);

			//dbTransaction.rollBackTransaction();

			throw new DbTransactionServiceException("Error occurred while deleting the record", dbDelete, "error");
		} catch (Exception e) {

			logger.error("Error occurred while deleting the record", e);

			//dbTransaction.rollBackTransaction();

			throw new DbTransactionServiceException("Error occurred while deleting the record", e, "error");

		}
	}

	
	public SDO insert(SDO entity, FilterCreator filterCreator) throws DbTransactionServiceException, Exception {

		logger.debug("Entered into service {0} ", "insert");
		try {
			logger.info("Before persisting...");

			entity = dbTransaction.persist(entity);

			logger.info("After persisting...");
		} catch (DbTransactionServiceException dbInsert) {

			logger.error("Error occurred while inserting the record", dbInsert);
			throw new DbTransactionServiceException("Error occurred while inserting the record", dbInsert, "error");
		} catch (Exception e) {

			logger.error("Error occurred while inserting the record", e);
			throw new DbTransactionServiceException("Error occurred while inserting the record", e, "error");

		}
		return entity;
	}

	
	public SDO update(SDO entity, FilterCreator filterCreator) throws DbTransactionServiceException, Exception {

		logger.debug("Entered into service {0} ", "update");
		try {
			logger.info("Before merging...");

			entity = dbTransaction.merge(entity);

			logger.info("After merging...");
		} catch (DbTransactionServiceException dbUpdate) {

			logger.error("Error occurred while updating the record", dbUpdate);
			throw new DbTransactionServiceException("Error occurred while updating the record", dbUpdate, "error");
		} catch (Exception e) {

			logger.error("Error occurred while updating the record", e);
			throw new DbTransactionServiceException("Error occurred while updating the record", e, "error");

		}
		return entity;
	}

	
	public List<SDO> selectList(String entity, FilterCreator filterCreator) throws DbTransactionServiceException {

		logger.debug("Entered into service {0} ", "selectlist");

		List<SDO> entityList = null;
		try {
			logger.info("Before selcting...");

			entityList = dbTransaction.getEntityList(entity, filterCreator);

			logger.info("After selecting...");

		} catch (DbTransactionServiceException dbSelect) {

			logger.error("Error occurred while selecting the record", dbSelect);
		} catch (Exception e) {

			logger.error("Error occurred while updating the record", e);
		}
		return entityList;
	}

}
